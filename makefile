!if $d(DEBUG)
TASM = tasm @pol_klaw.cfg -zi
TLINK = tlink /x /v
!else
TASM = tasm @pol_klaw.cfg
TLINK = tlink /x /t
!endif

.asm.obj:
	$(TASM) $*

pol_klaw.com: pol_klaw.obj setargv.obj
	$(TLINK) pol_klaw setargv
!if $d(DEBUG)
	tdstrip -c -s pol_klaw.exe
!endif

pol_klaw.obj: pol_klaw.asm pol_klaw.cfg

setargv.obj: setargv.asm pol_klaw.cfg

pol_klaw.cfg: makefile
	copy &&|
-d$$Model=TINY
-d$$Language=PASCAL
-d$$RegVars=0
-d__ALT__
-m
| pol_klaw.cfg

clean:
	del *.obj
	del *.cfg
!if $d(DEBUG)
	del *.tds
!endif

all: pol_klaw.com clean
