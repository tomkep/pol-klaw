IDEAL
INCLUDE "macros.inc"

PUBLIC _CType setargv, _CType argv, _CType argc

DATASEG
argv            dw      ?

IF @DataSize gt 0
                dw      ?
ENDIF

argc            dw      0



CODESEG
savedDI         dw      ?
savedSI         dw      ?

savedBP         dw      ?
returnaddress   dw      ?

IF @CodeSize gt 0
                dw      ?
ENDIF

savedDS         dw      ?



PROC _CType     setargv
                mov     [savedDS],ds
                pop     [word returnaddress]
IF @CodeSize gt 0
                pop     [word returnaddress + 2]
ENDIF
                mov     [savedDI],di
                mov     [savedSI],si
                mov     [savedBP],bp
                cld
                mov     ah,62h
                int     21h
                mov     bp,bx
                mov     es,bx
                mov     ah,30h
                int     21h
                mov     cx,1
                mov     bl,[es:80h]
                xor     bh,bh
                inc     bx
                cmp     al,3
                jb      @@2
                mov     es,[es:2Ch]
                xor     di,di
                xor     ax,ax
                mov     cx,8000h
        @@1:    dec     cx
                repne   scasb
                scasb
                jnz     @@1
                add     di,2
                mov     si,di
                mov     cx,7Fh
                repne   scasb
                xor     cx,7Fh
        @@2:    mov     dx,bx
                add     dx,cx
                inc     dx
                and     dx,0FFFEh
                sub     sp,dx
                mov     di,sp
                mov     dx,es
                mov     ds,dx
                mov     dx,ss
                mov     es,dx
                push    cx
                dec     cx
                rep     movsb
                xor     ax,ax
                stosb
                mov     ds,bp
                mov     si,81h
                mov     cx,bx
                mov     bx,ax
                mov     dx,1
        @@3:    lodsb
                cmp     al,9
                je      @@4
                cmp     al,32
                je      @@4
                cmp     al,13
                jne     @@5
        @@4:    xor     al,al
                or      ah,ah
                jz      @@6
                inc     dx
        @@5:    stosb
                mov     ah,al
                inc     bx
        @@6:    loop    @@3
IF @Model eq 7
                ASSUME  ds : @data
                mov     ax,@data
                mov     ds,ax
ELSE
                mov     ds,[savedDS]
ENDIF
                mov     [argc],dx
                inc     dx
IF @DataSize gt 0
                shl     dx,1
ENDIF
                shl     dx,1
                pop     cx
                mov     di,sp
                add     cx,bx
                sub     sp,dx
                mov     bp,sp
                xor     ax,ax
                mov     [word argv],bp
IF @DataSize gt 0
                mov     [word argv + 2],ss
ENDIF
        @@7:    mov     [bp],di
IF @DataSize gt 0
                mov     [bp+2],ss
                add     bp,4
ELSE
                add     bp,2
ENDIF
                repne   scasb
                jcxz    @@8
                jmp     short @@7
        @@8:    mov     [bp],ax
IF @DataSize gt 0
                mov     [bp + 2],ax
ENDIF
                mov     di,[savedDI]
                mov     si,[savedSI]
                mov     bp,[savedBP]
IF @Model eq 7
                mov     ds,[savedDS]
ENDIF
                jmp     [codeptr returnaddress]
ENDP            setargv

END
